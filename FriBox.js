if (!process.env.PORT) {
    process.env.PORT = 8080;
    //Kaj je port??? Vrata pomenijo številko, na katerem portu se nahaja storitev.
    //TO je vrstica, če kdo razvija lokalno na kompu.
}

var mime = require('mime');
var formidable = require('formidable');
var http = require('http');
var fs = require('fs-extra');
var util = require('util');
var path = require('path');


//Knjiznice, ki smo jih definirali

var dataDir = "./data/";

var streznik = http.createServer(function(zahteva, odgovor) {
    //Tole so funkcionalnosti našega strežnika, kle ga opredelimo, zdej ga mormo pognat
   if (zahteva.url == '/') {
       posredujOsnovnoStran(odgovor);
   } else if (zahteva.url == '/datoteke') { 
       posredujSeznamDatotek(odgovor);
   } else if (zahteva.url.startsWith('/brisi')) {
       //
       izbrisiDatoteko(odgovor, dataDir + zahteva.url.replace("/brisi", ""));
   } else if (zahteva.url.startsWith('/prenesi')) { 
       //Če se funkcionalnost zacne s prenesi, npr /prenesi+anakonda.jpg, tadrug del pa shrani datoteko
       posredujStaticnoVsebino(odgovor, dataDir + zahteva.url.replace("/prenesi", ""), "application/octet-stream");
   } else if(zahteva.url.startsWith('/poglej')){
       //Tole smo dodal funkcionalnost za poglej, kao da ne shran na racunalnik, in je skorej isto kt pr prenesi
       posredujStaticnoVsebino(odgovor, dataDir + zahteva.url.replace("/poglej", ""), "");
       
   } else if (zahteva.url == "/nalozi") {
       //File upload
       naloziDatoteko(zahteva, odgovor);
   } else {
       //Če ni nč od tega pol postrežemo statično vsebino.
       posredujStaticnoVsebino(odgovor, './public' + zahteva.url, "");
   }
});

//Preusmeri na zacetno stran.
function posredujOsnovnoStran(odgovor) {
    posredujStaticnoVsebino(odgovor, './public/fribox.html', "");
}

function posredujStaticnoVsebino(odgovor, absolutnaPotDoDatoteke, mimeType) {
        fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
            if (datotekaObstaja) {
                fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
                    if (napaka) {
                        //Posreduj napako, branje datoteke na strezniku
                        posredujNapako500(odgovor);
                    } else {
                        posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina, mimeType);
                    }
                })
            } else {
                //Posreduj napako, datoteka ne obstaja
                posredujNapako404(odgovor);
            }
        })
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina, mimeType) {
    if (mimeType == "") {
        odgovor.writeHead(200, {'Content-Type': mime.lookup(path.basename(datotekaPot))});    
    } else {
        odgovor.writeHead(200, {'Content-Type': mimeType});
    }
    
    odgovor.end(datotekaVsebina);
}

//Zgorej še kliče. 
function posredujSeznamDatotek(odgovor) {
    odgovor.writeHead(200, {'Content-Type': 'application/json'});
    fs.readdir(dataDir, function(napaka, datoteke) {
        if (napaka) {
            //Posreduj napako
            posredujNapako500(odgovor);
        } else {
            var rezultat = [];
            for (var i=0; i<datoteke.length; i++) {
                var datoteka = datoteke[i];
                var velikost = fs.statSync(dataDir+datoteka).size;    
                rezultat.push({datoteka: datoteka, velikost: velikost});
            }
            
            odgovor.write(JSON.stringify(rezultat));
            odgovor.end();      
        }
    })
}

function naloziDatoteko(zahteva, odgovor) {
    var form = new formidable.IncomingForm();
 
    form.parse(zahteva, function(napaka, polja, datoteke) {
        util.inspect({fields: polja, files: datoteke});
    });
 
    form.on('end', function(fields, files) {
        var zacasnaPot = this.openedFiles[0].path;
        var datoteka = this.openedFiles[0].name;
        fs.copy(zacasnaPot, dataDir + datoteka, function(napaka) {  
            if (napaka) {
                //Posreduj napako, streznik ne zna skopirat
                posredujNapako500(odgovor);
            } else {
                posredujOsnovnoStran(odgovor);        
            }
        });
    });
}

//6. korak - Napake:
function posredujNapako404(odgovor){
    odgovor.writeHead(404, {'Content-Type': 'text/plain'});
    odgovor.write('Napaka 404');
    odgovor.end();
}

function posredujNapako500(odgovor){
    odgovor.writeHead(500, {'Content-Type': 'text/plain'});
    odgovor.write('Napaka 500');
    odgovor.end();
}

function izbrisiDatoteko(odgovor, datoteka){
    odgovor.writeHead(200, {'Content-Type': 'text/plain'});
    fs.unlink(datoteka, function(napaka){
        if (napaka){
            posredujNapako404(odgovor);
        } else {
            odgovor.write("Datoteka izbrisana!");
            odgovor.end();
        }
    })    
}
//Tole požene strežnik
streznik.listen(process.env.PORT, function(){
    console.log("Streznik je pognan!");
})

